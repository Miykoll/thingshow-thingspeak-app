package com.example.miykoll.thingshow;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Toast toast;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        String toastText;
        //noinspection SimplifiableIfStatement
        switch(id){
            case R.id.menu_item_1: toastText = "Item 1";
                break;
            case R.id.menu_item_2: toastText = "Item 2";
                break;
            default: toastText = "Invalid item";
                break;

        }

        Toast.makeText(getApplicationContext(), toastText, Toast.LENGTH_SHORT).show();
        return super.onOptionsItemSelected(item);
    }

    public void addNewChannel(View view){
        Intent intent = new Intent(this, AddChannelActivity.class);
        startActivity(intent);
    }
}
